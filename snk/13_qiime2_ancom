###############################################################################
# Author: N. Fernandez
# Affiliation: IAC
# Aim: identify features that are differentially abundant across groups
# Date: 2018.11.20
# Run: snakemake -j [core] --use-conda
# Latest modification: 2018.11.20
# Todo:

###############################################################################
configfile: "config.yaml"

###############################################################################
# Wildcards
PROJECT = config["datasets"]["project"]
PRIMERS = config["datasets"]["primers"]

# Environment
QIIME2 = "../env/qiime2-2018.08.yaml"

# Params
METADATA = config["datasets"]["metadata"] # ancom

LEVEL = config["ancom"]["level"] # collapse

###############################################################################
rule all:
    input: 
        ancom = expand("out/{project}/{primers}/qiime2/visual/Ancom.qzv",
                       project = PROJECT,
                       primers = PRIMERS)

###############################################################################
rule ancom:
    # Aim: Apply Analysis of Composition of Microbiomes (ANCOM)
    # Use: qiime composition ancom [OPTIONS]
    conda:
        QIIME2
    params:
        metadata = METADATA
    input:
        composition_table = "out/{project}/{primers}/qiime2/ancom/CompTable.qza"
    output:
        ancom = "out/{project}/{primers}/qiime2/visual/Ancom.qzv"
    shell:
        "qiime composition ancom "
        "--i-table {input.composition_table} "
        "--m-metadata-file {params.metadata} "
        "--m-metadata-column Acronyme "
        "--o-visualization {output.ancom}"

###############################################################################
rule add_pseudocount:
    # Aim: Increment all counts in table by pseudocount
    # Use: qiime composition add-pseudocount [OPTIONS]
    conda:
        QIIME2
    input:
        collapsed_table = "out/{project}/{primers}/qiime2/ancom/ColTable.qza"
    output:
        composition_table = "out/{project}/{primers}/qiime2/ancom/CompTable.qza"
    shell:
        "qiime composition add-pseudocount "
        "--i-table {input.collapsed_table} "
        "--o-composition-table {output.composition_table}"

###############################################################################
rule taxa_collapse:
    # Aim: Collapse groups of features that have the same taxonomic assignment
    # Use: qiime taxa collapse [OPTIONS]
    conda:
        QIIME2
    params:
        level = LEVEL
    input:
        rarefied_table = "out/{project}/{primers}/qiime2/core/RarTable.qza",
        taxonomy = "out/{project}/{primers}/qiime2/taxonomy/Taxonomy.qza"
    output:
        collapsed_table = "out/{project}/{primers}/qiime2/ancom/ColTable.qza"
    shell:
        "qiime taxa collapse "
        "--i-table {input.rarefied_table} "
        "--i-taxonomy {input.taxonomy} "
        "--p-level {params.level} "
        "--o-collapsed-table {output.collapsed_table}"

###############################################################################
