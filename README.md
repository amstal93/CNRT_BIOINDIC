# metabarcoding

## install conda
1. Download the installer miniconda: [https://conda.io/miniconda.html](https://conda.io/miniconda.html)

2. In your Terminal window, run:
```shell
bash Miniconda3-latest-Linux-x86_64.sh
```
3. Follow the prompts on the installer screens.

## install snakemake with conda
```shell
conda install -c bioconda -c conda-forge snakemake
conda activate
```

## clone workflow into working directory
```shell
git clone http://gitlab.com/IAC-SolVeg/CNRT_BioIndicCNRT_BioIndic path/to/workdir
cd path/to/workdir
```

## edit config and workflow as needed
```shell
emacs config.yaml
```

## execute workflow, deploy software dependencies via conda
to run all snk/rules pipeline, with config settings (-n: drymod)
```shell
snakemake -j [core] -n
```
or to run specific rule (-n: drymod)
```shell
snakemake -j [core] --use-conda -s snk/xx_rule -n
```

│ \
├── .gitignore \
├── README.md \
├── LICENSE.md \
├── config.yaml \
├── Snakefile \
├── opt \
│&nbsp; &nbsp; &nbsp; ├── guilds_v1.1.py \
│&nbsp; &nbsp; &nbsp; └── script.R \
├── env \
│&nbsp; &nbsp; &nbsp; ├── quality.yaml \
│&nbsp; &nbsp; &nbsp; ├── qiime2.yaml \
│&nbsp; &nbsp; &nbsp; └── R.yaml \
├── snk \
│&nbsp; &nbsp; &nbsp; ├── 00_snakefile \
│&nbsp; &nbsp; &nbsp; └── 0N_snakefile \
├── conf \
│&nbsp; &nbsp; &nbsp; ├── 00_conf.yaml \
│&nbsp; &nbsp; &nbsp; └── 0N_conf.yaml \
├── inp \
│&nbsp; &nbsp; &nbsp; ├── reads \
│&nbsp; &nbsp; &nbsp; │&nbsp; &nbsp; &nbsp; ├── its2 \
│&nbsp; &nbsp; &nbsp; │&nbsp; &nbsp; &nbsp; └── v4 \
│&nbsp; &nbsp; &nbsp; └── qiime2 \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;├── manifest \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;│&nbsp; &nbsp; &nbsp; ├── manifest-negctrl-its2 \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;│&nbsp; &nbsp; &nbsp; ├── manifest-negctrl-v4 \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;│&nbsp; &nbsp; &nbsp; ├── manifest-bioindic-its2 \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;│&nbsp; &nbsp; &nbsp; └── manifest-bioindic-v4 \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;├── taxonomy \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;│&nbsp; &nbsp; &nbsp; ├── its2 \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;│&nbsp; &nbsp; &nbsp; │&nbsp; &nbsp; &nbsp; ├── sequence-database-version.fasta \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;│&nbsp; &nbsp; &nbsp; │&nbsp; &nbsp; &nbsp; └── taxonomy-database-version.txt \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;│&nbsp; &nbsp; &nbsp; └── v4 \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;├── sequence-database-version.fasta \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;└── taxonomy-database-version.txt \
│&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;└── metadata.tsv \
├── done \
├── out \
|&nbsp; &nbsp; &nbsp; ├── its2 \
|&nbsp; &nbsp; &nbsp; └── v4 \
└── log

